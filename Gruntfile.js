module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		less: {
			build: {
				files: {
					'css/style.css': 'styles/style.less'
				}
			}
		},
		ftp_push: {
		    demo: {
		    	options: {
		    		authKey: 'netology',
		    		host: 'university.netology.ru',
		    		dest: '/fbb-store/',
		    		port: 21
		    	},
		    	files: [{
		    		expand: true,
		    		cwd: '.',
		    		src: [
		    		      'index.html',
						  'about_company.html',
						  'book.html',
						  'order.html',
						  'script/script.js',
						  'script/script_book.js',
						  'script/script_order.js',
						  'script/search.js',
						  'script/additinal_functions.js',
						  'libl/normalize.css',
		    		      'css/style.css',
						  'README.md'
		    		]
		        }]
		    }
		 }
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-ftp-push');
	
	grunt.registerTask('default', ['less', 'ftp_push']);
	grunt.registerTask('start', ['less']);

};