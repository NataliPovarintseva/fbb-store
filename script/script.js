var searchRequest=decodeURIComponent(location.search.slice(1));
var bookJson;
var amountBookJson=8;
window.onload=function(){
   xmlHttp = createRequest();
   var elem = document.getElementById("bookList");
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4) {
            if(xmlHttp.status == 200) {
                bookJson=JSON.parse(xmlHttp.responseText);
                for(var i=0; i<amountBookJson; i++){
                    elem.appendChild(createtBook(bookJson, i));
                }
            }
            else{
              alert(xmlHttp.status);
            }
        }
    }
    xmlHttp.open('GET', 'http://university.netology.ru/api/book', true); 
    xmlHttp.send(null);

    var movable_book=document.getElementById("bookList");
    if(navigator.userAgent.indexOf("Firefox")==-1){
        event.target.ondragstart = function(event) {
            return false;
        };
    // }else{
    //     event.target.ondraggesture = function(event) {
    //         return false;
    //     };
    }
    movable_book.onmousedown = function(event){
        if(event.target.tagName=="IMG"){
            var book=parentClass(event.target,"book").firstChild;
            var bookNewevent=event;
            var parentElementBook =book.parentElement;
            var next_book;

            function moveAt(event) {
                book.style.left = event.pageX - book.offsetWidth / 2 + 'px';
                book.style.top = event.pageY - book.offsetHeight / 2 + 'px';
                book.style.display="none";
                if(document.elementFromPoint(event.clientX, event.clientY)!=movable_book){
                    if(document.elementFromPoint(event.clientX, event.clientY).classList.contains("book")){
                        next_book=document.elementFromPoint(event.clientX, event.clientY);
                    }else{
                        next_book=parentClass(document.elementFromPoint(event.clientX, event.clientY),"book");
                    }
                }
                book.style.display="inline-block";
                for(var i=0; i<amountBookJson; i++){
                    movable_book.children[i].style.borderLeft="none";
                }
                if(next_book!=undefined||next_book!=null){
                    next_book.style.borderLeft="5px solid red";
                }   
            }
            var stop;
            document.onmousemove = function(event) {
                if(stop==undefined){
                    book.style.position = 'absolute';
                    book.style.width ="252px";
                    book.style.cursor = "grabbing";
                    book.style.cursor = "-webkit-grabbing";
                    book.style.cursor = "-moz-grabbing";
                    document.body.appendChild(book);
                    book.style.zIndex=1000;
                }
                moveAt(event);
            }
            book.onmouseup = function(event) {
                book.style.position = 'static';
                book.style.width ="210px";
                book.style.cursor = "pointer";
                if (next_book==undefined||next_book==null) {
                    parentElementBook.insertBefore(book, parentElementBook.firstChild);
                    stop=1;
                    document.location.href = book.parentElement.children[1].firstChild.getAttribute("href");
                    return; 
                }
                document.onmousemove = null;
                book.onmouseup = null;
                parentElementBook.insertBefore(book, parentElementBook.firstChild);
                movable_book.insertBefore(parentClass(book, "book"), next_book);
                if(next_book!=undefined||next_book!=null){
                    next_book.style.borderLeft="none";
                }
            }
        }
    }

    document.getElementById("addBook").onclick=function () {
        if(amountBookJson<bookJson.length){
            if(amountBookJson+4<bookJson.length){
                amountBookJson+=4;
            }else{
                amountBookJson=bookJson.length;
            }
            var i=movable_book.children.length;
            for(; i<amountBookJson; i++){
                elem.appendChild(createtBook(bookJson, i));
            }
        }
    }
        
    var search=document.getElementById("search");
    if(searchRequest.length!=0){
        search.value=searchRequest;
    }
    search.onfocus=function(event){
        onkeyup=function(){
            if(search.value.length>=3){
                var textSearch=search.value.toLowerCase();
                for(var i=0; i<amountBookJson; i++){
                    if(movable_book.children[i].firstChild.getAttribute("alt").toLowerCase().indexOf(textSearch)!=-1){
                        movable_book.children[i].style.display="inline-block";
                    }else{
                        movable_book.children[i].style.display="none";
                    }
                }
            }else{
                for(var i=0; i<amountBookJson; i++){
                    movable_book.children[i].style.display="inline-block";
                }
            }
        }
        
    }
};

function createtBook(bookJson, i){
    var div = document.createElement('div');
    div.classList.add('book');
    var img = document.createElement('img');
    img.src=bookJson[i].cover.small;
    img.setAttribute("alt", bookJson[i].author.name+" "+bookJson[i].title);
    div.appendChild(img);
    var p = document.createElement('p');
    var a = document.createElement('a');
    a.setAttribute("href","book.html?"+bookJson[i].id)
    a.innerHTML=bookJson[i].info;
    p.appendChild(a);
    div.appendChild(p);
    if(bookJson[i].title.toLowerCase().indexOf(searchRequest)==-1&&bookJson[i].author.name.toLowerCase().indexOf(searchRequest)==-1){
        div.style.display="none";
    }
    return div;
}


