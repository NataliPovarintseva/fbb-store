var bookId=location.search.slice(1);
var currencyBook, price;
var divLeft, divRight;
document.addEventListener("DOMContentLoaded", ready);
function ready(){
    xmlHttp0 = createRequest();
    var elem = document.getElementById("bookInfo");
    xmlHttp0.onreadystatechange = function() { 
        if (xmlHttp0.readyState == 4) {
            if(xmlHttp0.status == 200) {
                var bookJson=JSON.parse(xmlHttp0.responseText);
                var img = document.createElement('img');
                img.src=bookJson.cover.large;
                document.getElementById("largeBook").appendChild(img);
                
                if(bookJson.description.length!=0){                  
                    var p = document.createElement('p');
                    p.classList.add("description");
                    p.innerHTML=bookJson.description;
                    elem.appendChild(p);
                }
                
                divLeft = document.createElement('div');
                divLeft.classList.add("divLeft");

                var imgAuthor = document.createElement('img');
                imgAuthor.src=bookJson.author.pic;
                divLeft.appendChild(imgAuthor);
                var pInfo = document.createElement('p');
                pInfo.innerHTML=bookJson.info;
                divLeft.appendChild(pInfo);
                var imgReviews = document.createElement('img');
                imgReviews.src=bookJson.reviews[1].author.pic;
                divLeft.appendChild(imgReviews);
                var pReviews = document.createElement('p');
                pReviews.innerHTML=bookJson.reviews[1].cite;
                divLeft.appendChild(pReviews);

                divRight = document.createElement('div');
                divRight.classList.add("divRight");

                var imgfeatures0 = document.createElement('img');
                imgfeatures0.src=bookJson.features[0].pic;
                divRight.appendChild(imgfeatures0);
                var pfeatures0 = document.createElement('p');
                pfeatures0.innerHTML=bookJson.features[0].title;
                divRight.appendChild(pfeatures0);
                var imgfeatures1 = document.createElement('img');
                imgfeatures1.src=bookJson.features[1].pic;
                divRight.appendChild(imgfeatures1);
                var pfeatures1 = document.createElement('p');
                pfeatures1.innerHTML=bookJson.features[1].title;
                divRight.appendChild(pfeatures1);
                if(document.documentElement.clientWidth+17>=1000){
                    elem.insertBefore(divLeft,document.getElementById("largeBook"));
                    elem.insertBefore(divRight,document.getElementById("largeBook"));
                }else{
                    elem.appendChild(divLeft);
                    elem.appendChild(divRight);
                }
                currencyBook=bookJson.currency;
                price=bookJson.price;
            }
            else{
              alert(xmlHttp.status);
            }
        }
    }
    xmlHttp0.open('GET', 'http://university.netology.ru/api/book/'+bookId, true); 
    xmlHttp0.send(null);

    xmlHttp1 = createRequest();
    var currency = document.getElementById("button").firstElementChild;
    xmlHttp1.onreadystatechange = function() { 
        if (xmlHttp1.readyState == 4) {
            if(xmlHttp1.status == 200) {
                var bookJson=JSON.parse(xmlHttp1.responseText);
                for(var i=0;i<bookJson.length;i++){
                    if(bookJson[i].ID==currencyBook){
                        currency.innerHTML=Math.round(price*bookJson[i].Value);
                        break;
                    }
                }                
            }
            else{
              alert(xmlHttp1.status);
            }
        }
    }
    xmlHttp1.open('GET', 'http://university.netology.ru/api/currency', true); 
    xmlHttp1.send(null);

    document.getElementById("button").onclick=function(){
        document.location.href="order.html?"+bookId;
    }
    window.onresize=function(){
        if(document.documentElement.clientWidth+17>=1000){
            elem.insertBefore(divLeft,document.getElementById("largeBook"));
            elem.insertBefore(divRight,document.getElementById("largeBook"));
        } else{
            elem.appendChild(divLeft);
            elem.appendChild(divRight);
        }
    }
    document.onmousemove=function(event){
        var eye=document.getElementById("largeBook").firstElementChild;
        var centerEyeX=(eye.getBoundingClientRect().right-eye.getBoundingClientRect().left)/2+eye.getBoundingClientRect().left;
        var centerEyeY=(eye.getBoundingClientRect().bottom-eye.getBoundingClientRect().top)/2+eye.getBoundingClientRect().top;
        var widthScreen=document.documentElement.clientWidth;
        var heightScreen = document.documentElement.clientHeight;
        var sizeEye=53;
        if(event.pageY-window.pageYOffset>heightScreen){
            var placeMouseY=heightScreen;
        }else{
            var placeMouseY=event.pageY-window.pageYOffset;
        }
        if(event.pageX>widthScreen){
            var placeMouseX=widthScreen;
        }else{
            var placeMouseX=event.pageX;
        }
        var coordinateX=placeMouseX-centerEyeX;
        var coordinateY=placeMouseY-centerEyeY;
        if(coordinateX>0&&coordinateY>0){//4 квадрат
            var proportionX=coordinateX/(coordinateX+coordinateY);
            var proportionY=coordinateY/(coordinateX+coordinateY);
            var moveX=sizeEye/(widthScreen-centerEyeX);
            var moveY=sizeEye/(heightScreen-centerEyeY);
            var top=Math.round(coordinateY*moveY);
            var left=Math.round(coordinateX*moveX);
            if(top+left>53){
                var sum=top+left-53;
                top-= proportionX*sum;
                left-=proportionY*sum;
            }
        }else if(coordinateX<0&&coordinateY<0){//1 квадрат
            var proportionX=coordinateX/(coordinateX+coordinateY);
            var proportionY=coordinateY/(coordinateX+coordinateY);
            var moveX=sizeEye/centerEyeX;
            var moveY=sizeEye/centerEyeY;
            var top=Math.round(coordinateY*moveY);
            var left=Math.round(coordinateX*moveX);
            if(top+left<-53){
                var sum=top+left+53;
                top+=-proportionX*sum;
                left+=-proportionY*sum;
            }
        }else if (coordinateX>0&&coordinateY<0){//2 квадрат
            var proportionX=coordinateX/(coordinateX-coordinateY);
            var proportionY=-coordinateY/(coordinateX-coordinateY);
            var moveX=sizeEye/(widthScreen-centerEyeX);
            var moveY=sizeEye/centerEyeY;
            var top=Math.round(coordinateY*moveY);
            var left=Math.round(coordinateX*moveX);
            if(left-top>53){
                var sum=left-top-53;
                top+=proportionX*sum;
                left-=proportionY*sum;
            }
        }else{//3 квадрат
            var proportionX=-coordinateX/(coordinateY-coordinateX);
            var proportionY=coordinateY/(coordinateY-coordinateX);
            var moveX=sizeEye/centerEyeX;
            var moveY=sizeEye/(heightScreen-centerEyeY);
            var top=Math.round(coordinateY*moveY);
            var left=Math.round(coordinateX*moveX);
            if(top-left>53){
                var sum=top-left-53;
                top-=proportionX*sum;
                left+=proportionY*sum;
            }
        }
        var top=top+58;
        var left=left+58;
        eye.firstElementChild.style.top=top+"px";
        eye.firstElementChild.style.left=left+"px";
    }
};

function createtBook(bookJson, i){
    var div = document.createElement('div');
    div.classList.add('book');
    var img = document.createElement('img');
    img.src=bookJson[i].cover.small;
    div.appendChild(img);
    var p = document.createElement('p');
    p.innerHTML=bookJson[i].info;
    div.appendChild(p);
    return div;
};

