var bookId=location.search.slice(1);
var currencyBook, price;
var deliveryJson, currencyJson, paymentJson;
var priceBook;
var needAdress;
document.addEventListener("DOMContentLoaded", ready);
function ready(){
    xmlHttp0 = createRequest();
    var elem = document.getElementById("bookOrder");
    xmlHttp0.onreadystatechange = function() { 
        if (xmlHttp0.readyState == 4) {
            if(xmlHttp0.status == 200) {
                var bookJson=JSON.parse(xmlHttp0.responseText);
                var title=elem.firstElementChild.lastElementChild;
                title.innerHTML=bookJson.title;
                title.setAttribute('href', "book.html?"+bookId);
                var img = elem.firstElementChild.nextElementSibling;
                img.src=bookJson.cover.small;
                currencyBook=bookJson.currency;
                price=bookJson.price;
            }
            else{
              alert(xmlHttp.status);
            }
        }
    }
    xmlHttp0.open('GET', 'http://university.netology.ru/api/book/'+bookId, true); 
    xmlHttp0.send(null);

    xmlHttp1 = createRequest();
    var currency = document.getElementById("currency");
    xmlHttp1.onreadystatechange = function() { 
        if (xmlHttp1.readyState == 4) {
            if(xmlHttp1.status == 200) {
                currencyJson=JSON.parse(xmlHttp1.responseText);
                for(var i=0;i<currencyJson.length;i++){
                    if(currencyJson[i].ID==currencyBook){
                        priceBook=Math.round(price*currencyJson[i].Value);
                        currency.innerHTML=priceBook;
                        break;
                    }
                }                
            }
            else{
              alert(xmlHttp1.status);
            }
        }
    }
    xmlHttp1.open('GET', 'http://university.netology.ru/api/currency', true); 
    xmlHttp1.send(null);

    xmlHttp2 = createRequest();
    var delivery = document.getElementById("bookOrder").children[2].children[0];
    xmlHttp2.onreadystatechange = function() { 
        if (xmlHttp2.readyState == 4) {
            if(xmlHttp2.status == 200) {
                deliveryJson=JSON.parse(xmlHttp2.responseText);
                for(var i=0;i<deliveryJson.length;i++){
                    var div = document.createElement('div');
                    div.classList.add("delivery");
                    var input =document.createElement('input');
                    input.setAttribute("type", "radio");
                    input.setAttribute("name", "delivery");
                    input.setAttribute("value", deliveryJson[i].id);
                    input.setAttribute("id", deliveryJson[i].id);
                    div.appendChild(input);
                    var label = document.createElement("label");
                    label.innerHTML=deliveryJson[i].name;
                    label.setAttribute("for", deliveryJson[i].id);
                    div.appendChild(label);
                    delivery.appendChild(div);
                }                
            }
            else{
              alert(xmlHttp2.status);
            }
        }
    }
    xmlHttp2.open('GET', 'http://university.netology.ru/api/order/delivery', true); 
    xmlHttp2.send(null);

    xmlHttp3 = createRequest();
    var payment = document.getElementById("bookOrder").children[2].children[1];
    xmlHttp3.onreadystatechange = function() { 
        if (xmlHttp3.readyState == 4) {
            if(xmlHttp3.status == 200) {
                paymentJson=JSON.parse(xmlHttp3.responseText);
                for(var i=0;i<paymentJson.length;i++){
                    var div = document.createElement('div');
                    div.classList.add("payment");
                    var input =document.createElement('input');
                    input.setAttribute("type", "radio");
                    input.setAttribute("name", "payment");
                    input.setAttribute("value", paymentJson[i].id);
                    input.setAttribute("id", paymentJson[i].id);
                    div.appendChild(input);
                    var label = document.createElement("label");
                    label.innerHTML=paymentJson[i].title;
                    label.setAttribute("for", paymentJson[i].id);
                    div.appendChild(label);
                    payment.appendChild(div);
                }                
            }
            else{
              alert(xmlHttp3.status);
            }
        }
    }
    xmlHttp3.open('GET', 'http://university.netology.ru/api/order/payment', true); 
    xmlHttp3.send(null);

    delivery.onclick=function(event){
        if(parentClass(event.target,"delivery")!=null){
            var activeDelivey=parentClass(event.target,"delivery").firstElementChild.getAttribute("id");
            for(var i=0;i<deliveryJson.length;i++){
                if(deliveryJson[i].id==activeDelivey){
                    needAdress=deliveryJson[i].needAdress;
                    var priceActiveDelivey=deliveryJson[i].price;
                    var currencyActiveDelivey=deliveryJson[i].currency;
                    break;
                }
            }
            if(needAdress==true){
                document.getElementById("adress").style.display="block";
            }else{
                document.getElementById("adress").style.display="none";
            }
            for(var i=0;i<currencyJson.length;i++){
                if(currencyJson[i].ID==currencyActiveDelivey){
                     currency.innerHTML=priceBook+Math.round(priceActiveDelivey*currencyJson[i].Value);
                     break;
                }
            }
            for(var i=0;i<paymentJson.length;i++){
                var activePayment=false;
                for(var j=0;j<paymentJson[i].availableFor.length;j++){
                    if(paymentJson[i].availableFor[j]==activeDelivey){
                        activePayment=true;
                    }
                }
                if(activePayment==true){
                    document.getElementById(paymentJson[i].id).removeAttribute("disabled");
                    document.getElementById(paymentJson[i].id).nextElementSibling.style.color="#000";
                }else{
                    document.getElementById(paymentJson[i].id).setAttribute("disabled","disabled");
                    document.getElementById(paymentJson[i].id).nextElementSibling.style.color="#808080";
                }
            }  
        }
    }

    var form=document.forms[0];
    var error=document.getElementById("error");
    form.elements.button.onclick=function(event){
        var order={};
        while(error.children.length!=0){
            error.removeChild(error.children[0]);
        }
        var regulName=/[а-яёА-ЯЁ]\s/;
        var regulTel=/^8+[0-9]/;
        var regulEmail=/[\.\-_A-Za-z0-9]+?@[\.\-A-Za-z0-9]+?[\ .A-Za-z0-9]{2,}/;
        if (form.elements.name.value.length==0) {
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Поле "Имя и Фамилия" не заполнено'));
            error.appendChild(p);
        }else if(!regulName.test(form.elements.name.value)){
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Поле "Имя и Фамилия" заполнено не корректно'));
            error.appendChild(p);
        }else{
            order.name=form.elements.name.value;
        }
        if (form.elements.tel.value.length==0) {
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Поле "Телефон" не заполнено'));
            error.appendChild(p);
        }else if(!regulTel.test(form.elements.tel.value)||form.elements.tel.value.length!=11){
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Поле "Телефон" заполнено не корректно'));
            error.appendChild(p);
        }else{
            order.phone=form.elements.tel.value;
        }
        if (form.elements.email.value.length==0) {
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Поле "Электронная почта" не заполнено'));
            error.appendChild(p);
        }else if(!regulEmail.test(form.elements.email.value)){
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Поле "Электронная почта" заполнено не корректно'));
            error.appendChild(p);
        }else{
            order.email=form.elements.email.value;
        }
        for(var i=0; i<form.elements.delivery.length;i++){
            var delivery=false;
            if(form.elements.delivery[i].checked){
                delivery=true;
                break;
            }
        }
        if(!delivery){
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Не выбран способ доставки'));
            error.appendChild(p);
        }else{
            order.delivery={};
            order.delivery.id=form.elements.delivery.value;
            if(needAdress==true){
                order.delivery.address=form.elements.mailAdress.value;
            }
            
        }
        for(var i=0; i<form.elements.payment.length;i++){
            var payment=false;
            if(!form.elements.payment[i].disabled){
                if(form.elements.payment[i].checked){
                    payment=true;
                    break;
                }
            }
        }
        if(!payment){
            var p=document.createElement("p");
            p.appendChild(document.createTextNode('Не выбран способ оплаты'));
            error.appendChild(p);
        }else{
            order.payment={};
            order.payment.id=form.elements.payment.value;
        }
        if(error.children.length==0){
            order.manager="taacrawom@gmail.com";
            order.book=bookId;
            order.comment=form.elements.comment.value;
            order.payment.currency="R01375";

            var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
            var xhr = new XHR();
            xhr.open('POST', 'http://university.netology.ru/api/order', true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                var otvetJson=JSON.parse(this.responseText);
                if(otvetJson.status=="success"){
                    document.getElementById("bookOrder").innerHTML="Заказ на книгу успешно оформлен! <br>Спасибо, что спасли книгу от сжигания в печи!"
                }else{
                    document.getElementById("bookOrder").innerHTML="Извините, но данная книга сгорела в печи. Наши работники уже печатают новый вариант. Повторите заказ позже."
                }  
            }
            xhr.onerror = function() {
                alert( 'Ошибка ' + this.status );
            }
            xhr.send(JSON.stringify(order));
        }
        return false;     
    }  
};


