
function createRequest(){
    var Request = false;
    if (window.XMLHttpRequest) {
        //Gecko-совместимые браузеры, Safari, Konqueror
        Request = new XMLHttpRequest();
    } else if (window.ActiveXObject){
        //Internet explorer
        try{
             Request = new ActiveXObject("Microsoft.XMLHTTP");
        }catch (CatchException){
             Request = new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    if (!Request){
        alert("Невозможно создать XMLHttpRequest");
    }
    return Request;
} 

function parentClass (elem, str) {
    var parent =elem.parentElement;
    if(elem.classList.contains(str)){
        return elem;
    }
    while(elem.tagName!="BODY"){
        if(parent.classList.contains(str)){
        return parent;
        }else{
            elem=parent;
            parent=elem.parentElement;
        }
    }
    return;
}
